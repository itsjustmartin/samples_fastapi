import os
from sqlmodel import create_engine, inspect

# ANSI color escape codes
COLOR_RESET = "\033[0m"
COLOR_BOLD = "\033[1m"
COLOR_BLUE = "\033[94m"
COLOR_GREEN = "\033[92m"
COLOR_YELLOW = "\033[93m"


def print_database_schema(database_url):

    engine = create_engine(database_url, echo=False)

    # Get the inspector
    inspector = inspect(engine)

    # Get all table names
    all_tables = inspector.get_table_names()

    print(f"{COLOR_BOLD}Tables in Your Database:{COLOR_RESET}")
    for table_name in all_tables:
        # Get columns for each table
        columns = inspector.get_columns(table_name)
        # Get primary key columns
        primary_keys = inspector.get_pk_constraint(
            table_name)['constrained_columns']
        # Get foreign keys
        foreign_keys = inspector.get_foreign_keys(table_name)
        fk_map = {fk['constrained_columns'][0]: fk for fk in foreign_keys}
        # Get indexes
        indexes = inspector.get_indexes(table_name)

        print(f"\t{COLOR_BOLD}{COLOR_BLUE}Table: {table_name}{COLOR_RESET}")
        for column in columns:
            column_name = column['name']
            col_details = (
                f"{COLOR_BOLD}Name:{COLOR_RESET} {column_name}, "
                f"{COLOR_BOLD}Type:{COLOR_RESET} {column['type']}, "
                f"{COLOR_BOLD}Nullable:{COLOR_RESET} {column['nullable']}, "
                f"{COLOR_BOLD}Default:{COLOR_RESET} {column.get('default')}"
            )

            # Check if the column is a primary key
            if column_name in primary_keys:
                col_details += f", {COLOR_BOLD}{COLOR_GREEN}Primary Key{COLOR_RESET}"

            # Check if the column is a foreign key
            if column_name in fk_map:
                fk = fk_map[column_name]
                col_details += (
                    f", {COLOR_BOLD}{COLOR_YELLOW}Foreign Key (References: {fk['referred_table']}"
                    f"({', '.join(fk['referred_columns'])})){COLOR_RESET}"
                )

            # Check if the column is indexed
            for index in indexes:
                if column_name in index['column_names']:
                    col_details += (
                        f", {COLOR_BOLD}Index (Name: {index['name']}, "
                        f"Unique: {index['unique']}){COLOR_RESET}"
                    )

            print(f"\t\t{col_details}")


if __name__ == "__main__":
    DATABASE_URL = os.getenv("DATABASE_URL")
    print_database_schema(DATABASE_URL)
