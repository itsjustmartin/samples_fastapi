# Define the directories for source code and tests
SRC_DIR := .
TEST_DIR := app/tests

# ? Install dependencies
install:
	pipenv install

# ? Run the FastAPI server for development
run:
	fastapi dev main.py

# ? Run tests
test:
	python -m pytest $(TEST_DIR) -p no:cacheprovider

coverage:
	coverage run -m pytest $(TEST_DIR) -p no:cacheprovider

report:
	coverage report
	coverage html

# ? Lint code using pylint
lint:
	pylint app

# ? Format code using isort
sort:
	isort app main.py

# ensure they are always executed
.PHONY: install run test lint sort coverage report
