"""FastAPI sample application."""

from contextlib import asynccontextmanager

from fastapi import FastAPI

from app.api.accounts import router as account_router
from app.api.posts import router as post_router
from app.config import settings
from app.core.database import create_db_and_tables, engine_dispose

tags_metadata = [
    {
        "name": "user",
        "description": "Operations related to user management, including creating, \
            reading users.",
    },
    {
        "name": "profile",
        "description": "Operations related to user profiles, \
            such as retrieving and updating profile information.",
    },
]


@asynccontextmanager
async def lifespan(application: FastAPI):  # pylint: disable=unused-argument
    create_db_and_tables()
    yield
    engine_dispose()

app = FastAPI(
    title=settings.app_name,
    lifespan=lifespan,
    description="This is a custom API with detailed descriptions and tags for users and profiles.",
    version="1.0.0",
    openapi_tags=tags_metadata,
)
# ? docs_url="/my-custom-docs" Customize the Swagger UI endpoint path


@app.get("/", summary="Root Path API", description="returns the root path message")
async def root():
    """Root path endpoint for the API."""
    return {"message": f"Hello from {settings.app_name}!"}

app.include_router(account_router)
app.include_router(post_router)

# * Run the application
if __name__ == "__main__":
    import uvicorn

    # uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
    uvicorn.run(app, host="127.0.0.1", port=8000)
