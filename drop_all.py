import os
from sqlmodel import create_engine, MetaData

if __name__ == '__main__':
    DATABASE_URL = os.getenv("DATABASE_URL")
    if DATABASE_URL is None:
        raise ValueError("DATABASE_URL environment variable is not set")

    engine = create_engine(DATABASE_URL)

    metadata = MetaData()
    metadata.reflect(bind=engine)

    metadata.drop_all(bind=engine)

    print("All tables dropped successfully.")
