# models.py
from typing import List

from sqlmodel import Field, Relationship, SQLModel


class Post(SQLModel, table=True):
    id: int | None = Field(default=None, primary_key=True, index=True)
    title: str = Field(max_length=255, nullable=False)
    content: str = Field(nullable=False)
    comments: List["Comment"] = Relationship(
        back_populates="post",
        sa_relationship_kwargs={
            "cascade": "all, delete",
        },
    )


class Comment(SQLModel, table=True):
    id: int | None = Field(default=None, primary_key=True, index=True)
    content: str
    post_id: int | None = Field(default=None, foreign_key="post.id")
    post: Post = Relationship(back_populates="comments")
