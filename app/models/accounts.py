import uuid as uuid_pkg
from datetime import datetime
from typing import Optional

from sqlmodel import Field, Relationship, SQLModel

from app.utils.hashlib import verify_password


class Profile(SQLModel, table=True):
    __tablename__ = "profiles"

    uuid: uuid_pkg.UUID = Field(
        default_factory=uuid_pkg.uuid4,
        primary_key=True,
        index=True,
        nullable=False,
    )
    owner_id: uuid_pkg.UUID = Field(foreign_key="users.uuid", nullable=False)

    email: str = Field(nullable=True)
    first_name: str = Field(max_length=50, nullable=True)
    last_name: str = Field(max_length=5, nullable=True)
    created_at: datetime = Field(
        default_factory=datetime.now, nullable=False
    )

    user: Optional["User"] = Relationship(back_populates="profile")


class User(SQLModel, table=True):
    __tablename__ = "users"

    uuid: uuid_pkg.UUID = Field(
        default_factory=uuid_pkg.uuid4,
        primary_key=True,
        index=True,
        nullable=False,
    )
    username: str = Field(
        min_length=3,
        unique=True,
        nullable=False,
        description="The username of the user. Must be unique and at least 3 characters long.",
    )
    password: str = Field(nullable=False)
    is_active: bool = Field(
        default=True,
        description="The password of the user. Cannot be null.",
    )

    profile: Profile | None = Relationship(
        back_populates="user",
        sa_relationship_kwargs={"uselist": False}  # ? One-to-one
    )

    def verify_password(self, plain_password: str) -> bool:
        return verify_password(plain_password, self.password)
