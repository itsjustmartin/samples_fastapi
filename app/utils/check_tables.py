from sqlalchemy import inspect
from sqlmodel import create_engine

SQLITE_FILE_NAME = "database.db"
SQLITE_URL = f"sqlite:///{SQLITE_FILE_NAME}"

connect_args = {"check_same_thread": False}
engine = create_engine(SQLITE_URL, echo=True, connect_args=connect_args)

all_tables = inspect(engine).get_table_names()

print("Tables in your database:")
for table_name in all_tables:
    # Get columns for each table
    table = inspect(engine).get_columns(table_name)
    print(f"\tTable: {table_name}")
    for column in table:
        # Print column details (name, type, nullable, etc.)
        print(
            f"\t\tColumn: {column['name']}, Type: {column['type']}, Nullable: {column['nullable']}")
