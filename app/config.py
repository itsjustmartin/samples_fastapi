import os

from dotenv import load_dotenv
from pydantic_settings import BaseSettings

load_dotenv()


class Settings(BaseSettings):
    app_name: str = "Snippet"

    # to get a string like this run:
    # openssl rand -hex 32
    secret_key: str = os.getenv(
        "SECRET_KEY", "c8194965e4e9d0e7982f035bf8696962a0d4fb9512d4305b655b6978c7867c61"
    )
    algorithm: str = "HS256"
    access_token_expire_minutes: int = 30


settings = Settings()
