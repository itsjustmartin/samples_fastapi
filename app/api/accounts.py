from typing import Annotated
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlmodel import Session, select

from app.core.database import get_session
from app.core.schema import (ProfileGet, ProfileUpdate, Token, UserCreate,
                             UserGet)
from app.models.accounts import Profile, User
from app.utils.hashlib import hash_password
from app.utils.jwt_authentication import (authenticate_user,
                                          create_access_token,
                                          get_current_active_user)

router = APIRouter()


@router.post("/login", tags=["user"])
async def login_for_access_token(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()], db: Session = Depends(get_session)
) -> Token:

    user = authenticate_user(form_data.username, form_data.password, db)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    access_token = create_access_token(data={"sub": user.username})
    #  TODO: update to `fastapi-users` to implement a refresh token

    return Token(access_token=access_token, token_type="bearer")


@router.post(
    "/register",
    response_model=UserGet,
    status_code=status.HTTP_201_CREATED,
    tags=["user"]
)
async def register(user: UserCreate, db: Session = Depends(get_session)) -> UserGet:
    existing_user = db.exec(
        select(User).where(User.username == user.username)
    ).first()
    if existing_user:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="Username already exists"
        )

    hashed_password = hash_password(user.password)
    user_db = User(username=user.username.lower(), password=hashed_password)
    profile_db = Profile(owner_id=user_db.uuid)

    db.add(user_db)
    db.add(profile_db)
    db.commit()
    db.refresh(user_db)

    return user_db.model_dump()


@router.get(
    "/profile/{user_id:uuid}",
    response_model=ProfileGet,
    status_code=status.HTTP_200_OK,
    tags=["profile"]
)
async def get_profile_by_id(
    user_id: UUID,
    db: Session = Depends(get_session)
) -> ProfileGet:

    profile = db.exec(
        select(Profile).where(Profile.owner_id == user_id)
    ).first()
    if not profile:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Profile not found"
        )

    return profile.model_dump()


@router.patch(
    "/profile/me",
    response_model=ProfileGet,
    status_code=status.HTTP_200_OK,
    tags=["profile"]
)
async def update_my_profile(
    profile_data: ProfileUpdate,
    current_user: Annotated[User, Depends(get_current_active_user)],
    db: Session = Depends(get_session)
) -> ProfileGet:

    profile = db.exec(
        select(Profile).where(Profile.owner_id == current_user.uuid)
    ).first()
    if not profile:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Profile not found"
        )

    for field, value in profile_data.model_dump().items():
        if value is not None:
            setattr(profile, field, value)

    db.commit()
    db.refresh(profile)

    return profile.model_dump()
