
from typing import List

from fastapi import APIRouter, Depends, HTTPException, status
from sqlmodel import Session, select

from app.core.database import get_session
from app.core.schema import (CommentCreate, CommentRead, CommentUpdate,
                             PostCreate, PostRead, PostUpdate)
from app.models.posts import Comment, Post

router = APIRouter()


@router.post(
    "/posts/",
    response_model=PostRead,
    status_code=status.HTTP_201_CREATED,
    tags=["posts"]
)
def create_post(post: PostCreate, session: Session = Depends(get_session)):
    db_post = Post.model_validate(post)
    session.add(db_post)
    session.commit()
    session.refresh(db_post)
    return db_post


@router.get("/posts/", response_model=List[PostRead], tags=["posts"])
def read_posts(session: Session = Depends(get_session)):
    posts = session.exec(select(Post)).all()
    return posts


@router.get("/posts/{post_id}", response_model=PostRead, tags=["posts"])
def read_post(post_id: int, session: Session = Depends(get_session)):
    post = session.get(Post, post_id)
    if not post:
        raise HTTPException(status_code=404, detail="Post not found")
    return post


@router.patch("/posts/{post_id}", response_model=PostRead, tags=["posts"])
def update_post(post_id: int, post_update: PostUpdate, session: Session = Depends(get_session)):
    post = session.get(Post, post_id)
    if not post:
        raise HTTPException(status_code=404, detail="Post not found")
    post_data = post_update.model_dump(exclude_unset=True)
    for key, value in post_data.items():
        setattr(post, key, value)
    session.add(post)
    session.commit()
    session.refresh(post)
    return post


@router.delete("/posts/{post_id}", status_code=status.HTTP_204_NO_CONTENT, tags=["posts"])
def delete_post(post_id: int, session: Session = Depends(get_session)) -> None:
    post = session.get(Post, post_id)
    if not post:
        raise HTTPException(status_code=404, detail="Post not found")
    session.delete(post)
    session.commit()


@router.post(
    "/posts/{post_id}/comments/",
    response_model=CommentRead,
    status_code=status.HTTP_201_CREATED,
    tags=["comments"],
)
def create_comment(post_id: int, comment: CommentCreate, session: Session = Depends(get_session)):
    db_post = session.get(Post, post_id)
    if not db_post:
        raise HTTPException(status_code=404, detail="Post not found")
    db_comment = Comment.model_validate(comment)
    db_comment.post_id = post_id
    session.add(db_comment)
    session.commit()
    session.refresh(db_comment)
    return db_comment


@router.get("/posts/{post_id}/comments/", response_model=List[CommentRead], tags=["comments"])
def read_comments(post_id: int, session: Session = Depends(get_session)):
    db_post = session.get(Post, post_id)
    if not db_post:
        raise HTTPException(status_code=404, detail="Post not found")
    comments = session.exec(select(Comment).where(
        Comment.post_id == post_id)).all()
    return comments


@router.patch("/comments/{comment_id}", response_model=CommentRead, tags=["comments"])
def update_comment(
    comment_id: int,
    comment_update: CommentUpdate,
    session: Session = Depends(get_session)
):
    comment = session.get(Comment, comment_id)
    if not comment:
        raise HTTPException(status_code=404, detail="Comment not found")
    comment_data = comment_update.model_dump(exclude_unset=True)
    for key, value in comment_data.items():
        setattr(comment, key, value)
    session.add(comment)
    session.commit()
    session.refresh(comment)
    return comment


@router.delete("/comments/{comment_id}", status_code=status.HTTP_204_NO_CONTENT, tags=["comments"])
def delete_comment(comment_id: int, session: Session = Depends(get_session)) -> None:
    comment = session.get(Comment, comment_id)
    if not comment:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Comment not found")
    session.delete(comment)
    session.commit()
