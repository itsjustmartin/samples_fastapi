import pytest
from fastapi.testclient import TestClient
from sqlmodel import Session

from app.models.posts import Post


@pytest.fixture(scope="function", autouse=True)
def seed(db: Session):
    db.add_all([
        Post(title="Test Post 1", content="Content of Test Post 1"),
        Post(title="Test Post 2", content="Content of Test Post 2")
    ])
    db.commit()
    yield


def test_create_post(client: TestClient):
    post_data = {"title": "Test Post", "content": "This is a test post."}
    response = client.post("/posts/", json=post_data)
    assert response.status_code == 201
    assert response.json()["title"] == "Test Post"
    assert response.json()["content"] == "This is a test post."


def test_read_posts(client: TestClient):
    response = client.get("/posts/")
    assert response.status_code == 200
    assert isinstance(response.json(), list)


def test_read_post(client: TestClient):
    # Assuming post with ID 1 exists
    response = client.get("/posts/1")
    assert response.status_code == 200
    assert response.json()["id"] == 1


def test_update_post(client: TestClient):
    # Assuming post with ID 1 exists
    post_data = {
        "title": "Updated Test Post",
        "content": "This is an updated test post."
    }
    response = client.patch("/posts/1", json=post_data)
    assert response.status_code == 200
    assert response.json()["title"] == "Updated Test Post"
    assert response.json()["content"] == "This is an updated test post."


def test_delete_post(client: TestClient):
    # Assuming post with ID 1 exists
    response = client.delete("/posts/1")
    assert response.status_code == 204
