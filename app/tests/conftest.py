import pytest
from fastapi.testclient import TestClient
from sqlmodel import Session, SQLModel, create_engine

from app.core.database import get_session
from main import app

TEST_SQLITE_URL = "sqlite:///:memory:"


@pytest.fixture(scope="module", name="engine")
def fixture_engine():
    engine = create_engine(
        TEST_SQLITE_URL,
        connect_args={"check_same_thread": False},
        echo=True
    )
    SQLModel.metadata.create_all(bind=engine)
    yield engine.connect()
    SQLModel.metadata.drop_all(bind=engine)


@pytest.fixture(scope="function", name="db")
def fixture_db(engine):
    with Session(engine) as session:
        yield session


@pytest.fixture(scope="function", name="client")
def fixture_client(db):
    app.dependency_overrides[get_session] = lambda: db
    with TestClient(app) as client:
        yield client
    app.dependency_overrides.clear()
