# pylint: disable=unused-import
import re
import uuid as uuid_pkg
from datetime import datetime
from typing import List

from fastapi import HTTPException
from pydantic import BaseModel, EmailStr, field_validator


class UserBase(BaseModel):
    username: str


class UserCreate(UserBase):
    password: str

    # ? uncoment this for apply password validation
    # @field_validator("password")
    # @classmethod
    # def validate_password(cls, password: str) -> str:
    #     pattern = r"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)(.{8,})"
    #     if not re.match(pattern, password):
    #         raise HTTPException(
    #             status_code=422,  # Unprocessable Entity
    #             detail="Password must be at least 8 characters and include a lowercase letter, \
    #                 an uppercase letter, a number, and a special character."
    #         )
    #     return password

    class ConfigDict:
        json_schema_extra = {
            "examples": [
                {
                    "username": "johndoe",
                    "password": "MySecureP@ssw0rd",
                }
            ]
        }


class UserGet(UserBase):
    is_active: bool | None
    uuid: uuid_pkg.UUID


class ProfileBase(BaseModel):
    first_name: str | None
    last_name: str | None
    email: EmailStr | None


class ProfileGet(ProfileBase):
    uuid: uuid_pkg.UUID
    owner_id: uuid_pkg.UUID
    created_at: datetime

    class ConfigDict:
        from_attributes = True
        json_schema_extra = {
            "examples": [
                {
                    "uuid": "e1b9b1d0-9f0a-4c8a-a9d4-bd833df445ed",
                    "owner_id": "c1a23b4c-5f1d-4f1d-8c7d-6e833aa9450d",
                    "email": "foo@example.com",
                    "first_name": "Foo",
                    "last_name": "Bar",
                    "created_at": "2023-01-01T00:00:00Z",
                }
            ]
        }


class ProfileUpdate(ProfileBase):
    pass


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None


class CommentBase(BaseModel):
    content: str


class CommentCreate(CommentBase):
    pass


class CommentRead(CommentBase):
    id: int
    post_id: int

    class ConfigDict:
        from_attributes = True


class CommentUpdate(BaseModel):
    content: str | None


class PostBase(BaseModel):
    title: str
    content: str


class PostCreate(PostBase):
    pass


class PostRead(PostBase):
    id: int
    comments: List[CommentRead]

    class ConfigDict:
        from_attributes = True


class PostUpdate(BaseModel):
    title: str | None
    content: str | None
