from typing import Generator

from sqlmodel import Session, SQLModel, create_engine

SQLITE_FILE_NAME = "database.db"
SQLITE_URL = f"sqlite:///{SQLITE_FILE_NAME}"

connect_args = {"check_same_thread": False}
engine = create_engine(SQLITE_URL, echo=True, connect_args=connect_args)


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)


def engine_dispose():
    engine.dispose()


def get_session() -> Generator[Session, None, None]:
    with Session(engine) as session:
        yield session

# !
# def get_db():
#     db = Session(engine)
#     try:
#         yield db
#     finally:
#         db.close()
